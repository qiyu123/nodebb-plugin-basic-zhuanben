<<<<<<< HEAD
# QuickStart for NodeBB
本项目是 NodeBB 插件的快速开始轮子， 您可以通过本项目来快速初始化您的 NodeBB 插件项目。创建本项目是为了促进 NodeBB 追随 ES 的现代化。
您可以通过本项目来使用任何的 ES6+ 语法（如果不考虑引擎支持）。
有关 NodeBB 的 Hooks， 以及其他的开发信息。请访问 NodeBB 社区 或者 Github 中的 Wiki。

> 请注意： NodeBB v1.13.x 系统函数库，钩子已经基本支持 Promise 回调。 所以， 本库移除 Callbackify 部分。

# 开始
1. 克隆本项目至本地。
2. 删除项目中的 `.git` 文件夹
3. 使用 `git init` 初始化一个属于你的新仓库。
4. 执行 `yarn` 安装依赖（推荐使用 `yarn`)
4. 修改... 提交

# 调试
1. 编写插件，保存。
2. `yarn link` 来软链接插件
3. 在 NodeBB 目录下执行 `yarn link nodebb-plugin-quickstart` 来引入插件
4. `./nodebb build && ./nodebb dev` 启动 NodeBB 开发环境 

# 使用说明
编写完成后，别忘记为插件添加使用说明和屏幕截图哦！
=======
# nodebb-plugin-basic-zhuanben

#### 介绍
{**以下是码云平台说明，您可以替换此简介**
码云是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用码云实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
>>>>>>> 96df004407980393bb8bfa75fc6fa539b616aa34
