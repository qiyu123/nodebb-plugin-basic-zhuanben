<form role="form" class="basic-settings">





	<div class="row">
		<div class="col-sm-2 col-xs-12 settings-header">常规</div>
		<div class="col-sm-10 col-xs-12">
			<div class="checkbox">
				<label for="removeSlug" class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
					<input type="checkbox" class="mdl-switch__input" id="removeSlug" name="removeSlug" />
					<span class="mdl-switch__label">移除目录中的Slug,修改主题的Slug为post</span>
				</label>
			</div>

			<div class="checkbox">
				<label for="showCC" class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
					<input type="checkbox" class="mdl-switch__input" id="showCC" name="showCC" />
					<span class="mdl-switch__label">在主题页面的第一帖下显示创作许可协议</span>
				</label>
			</div>


		</div>
	</div>
</form>

<button id="save" class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
	<i class="material-icons">save</i>
</button>
