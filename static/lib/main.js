"use strict";

/* globals document, $ */

/*
		这个文件告诉我们：如何引入一个客户端 js 脚本
		在 `plugin.json` 中，你可以发现本文件列在 "scripts" 字段中。
		那个数组的意义是告知 NodeBB 构建时，需要引入并优化的客户端 js 脚本。

		这些方法你很可能会用到：

		$(document).ready();	  当 DOM 加载完毕时会触发。
		$(window).on('action:ajaxify.end', function(data) { ... });		注："data" 包含 "url" 
		Ajax 请求完成后触发，更明确的表述： Ajax操作完成， 并切换路由后触发。
	*/

//console.log('nodebb-plugin-basic: 已载入');
// 注意：这个会在着陆页触发。

$(document).ready(function() {
/*   $(window).on("action:posts.loaded", function(ev, data) {}); */
/* 
	var url = window.location.protocol + '//' + window.location.host + config.relative_path; */
  // Page Loaded ,We don't need to hook posts loaded.


  if(config.showCC){

    $(window).on("action:ajaxify.end", function() {

      // Check if topic
      if (ajaxify.data.template.topic) {
  
        var posts = '[component="post"]';
        var post = $(posts).first();
        if (post) {
          app.parseAndTranslate("partials/topic/cc", {}, function(tpl) {
            $(tpl).appendTo(post.find('[component="post/content"]'));
          });
        }
      }
    });
  }


});
