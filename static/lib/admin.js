'use strict';

/* 全局定义了 $, app, socket, define */

define('admin/plugins/basic', ['settings', 'admin/modules/colorpicker'], function (Settings,colorpicker) {
	var ACP = {};

	ACP.init = function () {
    Settings.load('basic', $('.basic-settings'));


		$('#save').on('click', function () {
			Settings.save('basic', $('.basic-settings'), function () {
				app.alert({
					type: 'success',
					alert_id: 'basic-saved',
					title: 'Settings Saved',
					message: '点击以重启你的 NodeBB 使变动生效。',
					clickfn: function () {
						socket.emit('admin.reload');
					},
				});
			});
		});
	};




	return ACP;
});
